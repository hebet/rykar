/// @description Update Camera
// You can write your code in this editor


//Uptade destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
}

// Uptade Object Position
x += (xTo - x) /15;
y += (yTo - y) /15;

//Keep Camera center  inside room

x = clamp(x, viewWidthHaft, room_width-viewWidthHaft);
y = clamp(y, viewHeigthHalf, room_height-viewHeigthHalf);

//Screenshake
x += random_range(-shakeRemain,shakeRemain);
y += random_range(-shakeRemain,shakeRemain);

shakeRemain = (max(0, shakeRemain - (1/shakeLength) * shakeMagnitude));

camera_set_view_pos(cam,x - viewWidthHaft,y - viewHeigthHalf);
